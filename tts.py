#!/usr/bin/env python3
import cv2
import tkinter
import librosa
import numpy as np
import sounddevice as sd
from PIL import Image, ImageTk


# spectrogram
win_size = 45   # ms
overlap = 3/4   # percentage
fs = 16000
# Griffin Lim
n_iter = 10


class Spectro:
    def __init__(self, circle_size=2, win_size=45, dyn=50, overlap=3/4,
                 fs=16000, n_iter=10, fps=24):
        self.circle_size = circle_size
        self.fs = fs
        self.n_iter = n_iter
        self.circle_size = circle_size
        self.fps = fps
        self.dyn = dyn

        self.win_points = int(np.round(win_size*1e-3 * fs))
        self.n_fft = int(2**np.ceil(np.log2(self.win_points)))
        self.hop_points = int(np.floor(self.win_points*(1-overlap)))
        self.tmax = 10  # s, should be computed
        self.fmax = self.fs/2
        self.rec_stft = None  # STFT of a recorded signal

        self.old_x, self.old_y = None, None
        self.x, self.y = None, None
        self.sig = np.zeros(self.tmax*self.fs)
        stft = librosa.stft(self.sig, n_fft=self.n_fft,
                            hop_length=self.hop_points,
                            win_length=self.win_points)
        self.shape = stft.shape
        self.image = np.ones(self.shape)*255  # drawing STFT
        self.mask = np.ones(self.shape)       # masking recorded STFT
        self.sig_flag = False
        self.is_drawing = False

        self.stopped = True     # not playing a sound
        self.recording = False  # not recording
        self.scheduled = []     # list of scheduled events

        self.plot_red_bar = False

        self.root = tkinter.Tk()
        self.root.wm_title("Trace Thy Sound")
        xmax = self.root.winfo_screenwidth()
        ymax = self.root.winfo_screenheight()

        self.menu_height = 40
        self.axes_size = 40
        self.border_size = 2

        self.rubber = tkinter.IntVar()
        self.rubber.set(False)

        # prepare the menu
        self.menu = tkinter.Frame(self.root, height=self.menu_height,
                                  width=xmax, bg="lightgrey")
        self.menu.place(x=0, y=ymax-self.menu_height)

        self.check_rubber = tkinter.Checkbutton(master=self.menu,
                                                text="Eraser",
                                                variable=self.rubber,
                                                onvalue=True,
                                                offvalue=False)
        self.check_rubber.place(rely=0.25, relx=0.05)

        self.button_clear = tkinter.Button(master=self.menu, text="Clear",
                                           command=self.clear)
        self.button_clear.place(rely=0.15, relx=0.15)

        self.scale_grey = tkinter.Scale(master=self.menu, orient="horizontal",
                                        from_=0, to=1, resolution=0.1,
                                        length=100, command=self.change_grey)
        self.label_grey_left = tkinter.Label(self.menu, text="White")
        self.label_grey_right = tkinter.Label(self.menu, text="Black")
        self.label_grey_left.place(rely=0.20, x=0.25*xmax-40)
        self.label_grey_right.place(rely=0.20, x=0.25*xmax+100)
        self.scale_grey.place(rely=-0.10, relx=0.25)
        self.scale_grey.set(1)  # set to black

        self.scale_size = tkinter.Scale(master=self.menu, orient="horizontal",
                                        from_=1, to=20, resolution=1,
                                        length=100, command=self.change_size)
        self.label_size_left = tkinter.Label(self.menu, text="Small")
        self.label_size_right = tkinter.Label(self.menu, text="Big")
        self.label_size_left.place(rely=0.25, x=0.35*xmax-40)
        self.label_size_right.place(rely=0.25, x=0.35*xmax+100)
        self.scale_size.place(rely=-0.1, relx=0.35)
        self.scale_size.set(self.circle_size)

        self.button_play = tkinter.Button(master=self.menu, text="Play",
                                          command=self.play_sound)
        self.button_play.place(rely=0.15, relx=0.65)

        self.button_stop = tkinter.Button(master=self.menu, text="Stop",
                                          command=self.stop_sound)
        self.button_stop.place(rely=0.15, relx=0.75)

        self.button_quit = tkinter.Button(master=self.menu, text="Quit",
                                          command=self.quit)
        self.button_quit.place(rely=0.15, x=xmax-80)

        self.button_rec = tkinter.Button(master=self.menu, text="Record",
                                         command=self.record_sound)
        self.button_rec.place(rely=0.15, relx=0.85)

        self.root.attributes('-fullscreen', True)

        # Canvas for drawing axes and title
        self.graph = tkinter.Canvas(self.root, height=ymax-self.menu_height,
                                    width=xmax, bg="white")
        self.graph.place(x=0, y=0)

        # Add a background black square to add a border around the spectrogram
        width = xmax-3*self.axes_size
        height = ymax-self.menu_height-3*self.axes_size
        self.border = tkinter.Frame(self.root, width=width+2*self.border_size,
                                    height=height+2*self.border_size,
                                    bg="black")
        self.border.place(x=2*self.axes_size-self.border_size,
                          y=self.axes_size-self.border_size)
        # Spectrogram
        self.spectro = tkinter.Label()
        self.spectro.place(x=2*self.axes_size, width=width, y=self.axes_size,
                           height=height)

        self.graph.bind('<Motion>', self.draw_frame)
        self.spectro.bind('<Motion>', self.draw_spectro)
        self.graph.bind('<ButtonPress>', self.click_frame)
        self.spectro.bind('<ButtonPress>', self.click_spectro)
        self.graph.bind('<ButtonRelease>', self.unclick_frame)
        self.spectro.bind('<ButtonRelease>', self.unclick_spectro)
        self.root.bind('<Key>', self.key_binding)
        self.root.bind('<Configure>', self.redraw)

        self.image = cv2.resize(self.image, (xmax-3*self.axes_size,
                                ymax-self.menu_height-3*self.axes_size))
        self.photoimage = ImageTk.PhotoImage(Image.fromarray(self.image))
        self.spectro.config(image=self.photoimage)

        # add proper axes
        self.draw_axes(width, height)
        self.graph.create_text(self.axes_size/2, ymax/2-self.axes_size,
                               text="Frequency (Hz)", angle=90)
        self.graph.create_text(xmax/2+self.axes_size,
                               ymax-self.menu_height-self.axes_size/2,
                               text="Time (s)")
        self.graph.create_text(xmax/2, self.axes_size/2,
                               text="Trace Thy Sound!")

    def draw_axes(self, width, height):
        # 0 to 10s
        xticks = np.round(np.linspace(0, width, 11)) + 2*self.axes_size - 1
        for i, x in enumerate(xticks):
            self.graph.create_line(x, self.axes_size+height, x,
                                   self.axes_size+height+10)
            self.graph.create_text(x, self.axes_size+height+20, text=str(i))

        fmax = 100*np.floor(self.fs/200)
        df = int(fmax/10)
        freq_height = height * fmax / (self.fs/2)
        yticks = np.round(np.linspace(0, -freq_height, 11)) + (self.axes_size
                                                               + height)
        for i, y in enumerate(yticks):
            self.graph.create_line(2*self.axes_size-10, y, 2*self.axes_size, y)
            self.graph.create_text(2*self.axes_size-30, y, text=str(i*df))

    def clear(self):
        self.stop_sound()
        self.rec_stft = None
        self.sig = np.zeros(self.tmax*self.fs)
        self.image = np.ones(self.image.shape)*255
        self.mask = np.ones(self.image.shape)
        self.redraw()

    def redraw(self, event=None):
        if self.rec_stft is None:
            tmp = np.uint8(self.image)
        else:
            self.stft_image[self.mask == 0] = 255  # removed parts in white
            tmp = np.uint8(self.image * self.stft_image / 255)
        if self.plot_red_bar:
            tmp = np.dstack((tmp, tmp, tmp))  # 3 channels image: RGB
            cv2.line(tmp, (self.red_bar_x, 0), (self.red_bar_x, tmp.shape[1]),
                     (255, 0, 0), 1)
        tmp = Image.fromarray(tmp)
        self.photoimage = ImageTk.PhotoImage(tmp)
        self.spectro.config(image=self.photoimage)

    def click_spectro(self, event):
        self.click(event.x, event.y, event.num)

    def click_frame(self, event):
        self.click(event.x-2*self.axes_size, event.y-self.axes_size, event.num)

    def click(self, x, y, button):
        if button == 3:
            self.rubber.set(not(self.rubber.get()))
        else:
            self.is_drawing = True
            self.old_x, self.old_y = x, y

    def unclick_spectro(self, event):
        self.unclick(event.x, event.y)

    def unclick_frame(self, event):
        self.unclick(event.x-2*self.axes_size, event.y-self.axes_size)

    def unclick(self, x, y):
        self.draw(x, y)
        self.is_drawing = False

    def draw_spectro(self, event):
        self.draw(event.x, event.y)

    def draw_frame(self, event):
        self.draw(event.x-2*self.axes_size, event.y-self.axes_size)

    def draw(self, x, y):
        if self.is_drawing:
            colour = 255 if self.rubber.get() else (1-self.grey)*255
            cs = self.circle_size+9 if self.rubber.get() else self.circle_size
            cv2.line(self.image, (x, y), (self.old_x, self.old_y), colour, cs)
            if self.rec_stft is not None and self.rubber.get():
                # masking the STFT of the recorded signal
                cv2.line(self.mask, (x, y), (self.old_x, self.old_y), 0, cs)
            self.old_x, self.old_y = x, y
            self.redraw()
            self.sig_flag = True

    def change_grey(self, data):
        self.grey = float(data)

    def change_size(self, data):
        self.circle_size = int(data)

    def move_red_bar(self):
        self.plot_red_bar = True
        self.red_bar_x_pc = 0
        self.red_bar_x = 0
        self.scheduled = [ self.graph.after(round(i/self.fps*1000), self.move_one_step_bar)
                          for i in range(10*self.fps+1) ]

    def move_one_step_bar(self):
        self.scheduled.pop(0)
        self.redraw()
        self.red_bar_x_pc += 1/(10*self.fps)
        self.red_bar_x = int(self.image.shape[1] * self.red_bar_x_pc)
        if self.red_bar_x_pc >= 1:  # end of the screen: erase
            self.remove_red_bar()
            self.stopped = True

    def stop_red_bar_and_scheduled(self):
        for id in self.scheduled:
            self.graph.after_cancel(id)
        self.scheduled = []
        self.remove_red_bar()

    def remove_red_bar(self):
        self.plot_red_bar = False
        self.redraw()

    def spectro2sound(self):
        Image = cv2.resize(self.image, (self.shape[1], self.shape[0]))
        S = 1-Image[::-1, :]/255  # frequencies on bottom
        if np.sum(S) > 1e-3:  # non-zero, in case of recorded signal only
            S = 10**(S)
            S -= S.min()
            self.sig = librosa.griffinlim(S, n_iter=self.n_iter,
                                          hop_length=self.hop_points,
                                          win_length=self.win_points)
            self.sig = self.normalize_signal_size(self.sig)
        else:  # to clear a potential previous inverted STFT
            self.sig = np.zeros(self.tmax*self.fs)
        if self.rec_stft is not None:
            mask = cv2.resize(self.mask, (self.shape[1], self.shape[0]))
            mask = mask[::-1, :]  # frequencies on bottom
            rec_stft = self.rec_stft * mask  # t-f filtering
            rec_sig = librosa.istft(rec_stft, hop_length=self.hop_points,
                                    win_length=self.win_points)
            rec_sig = self.normalize_signal_size(rec_sig)
            self.sig += rec_sig
        self.sig_flag = False

    def normalize_signal_size(self, s):
        # normalize amplitude and ensures length is tmax × fs
        sig = np.zeros(self.tmax*self.fs)  # length is even
        if len(s) > len(sig):
            sig = s[:len(sig)]
        else:
            if len(s) % 2:  # odd number
                s = s[:-1]  # even number
            sig = np.pad(s, int((len(sig)-len(s))/2))
        return sig/np.abs(sig).max()

    def play_sound(self):
        if self.sig_flag:
            self.spectro2sound()
        if np.sum(np.abs(self.sig)):
            if not self.stopped:
                self.stop_sound()
            self.stopped = False
            self.move_red_bar()
            # resampling to avoid strange zero-holder-block effect
            sd.play(librosa.resample(self.sig, orig_sr=self.fs, target_sr=44100), 44100)

    def stop_sound(self):
        sd.stop()
        self.stop_red_bar_and_scheduled()
        self.stopped = True
        if self.recording:
            self.sound2img()

    def record_sound(self):
        if not self.stopped:
            self.stop_sound()
        self.stopped = False
        self.recording = False
        self.clear()
        self.move_red_bar()
        self.rec_sig = sd.rec(frames=int(self.tmax*self.fs),
                              samplerate=self.fs, channels=1)
        self.scheduled.append( self.graph.after(round(self.tmax*1000), self.sound2img) )

    def sound2img(self):
        sd.wait()  # in case self.graph.after is too short, wait for the end of recording
        self.recording = False
        self.rec_sig[ np.isnan(self.rec_sig) ] = 0  # changes NaN in 0
        self.rec_sig = self.rec_sig/(np.abs(self.rec_sig.flatten()).max()+1e-20)
        self.rec_stft = librosa.stft(self.rec_sig.flatten(), n_fft=self.n_fft,
                                     hop_length=self.hop_points,
                                     win_length=self.win_points)
        self.stft2image()

    def stft2image(self):
        S = np.abs(self.rec_stft)
        Stmp = 10*np.log10(np.abs(S[::-1, :])**2+1e-15)
        StmpMax = Stmp.max()
        Stmp[Stmp < (StmpMax - self.dyn)] = StmpMax - self.dyn
        Stmp = (Stmp - StmpMax + self.dyn)/self.dyn
        Image = cv2.resize(Stmp, (self.image.shape[1], self.image.shape[0]))
        self.stft_image = 255*(1-Image)
        self.redraw()
        self.sig_flag = True

    def quit(self):
        self.root.quit()
        self.root.destroy()

    def key_binding(self, event):
        print(event.keysym)
        if event.keysym in ["q", "Q"]:
            self.quit()
        if event.keysym in ["p", "P"]:
            self.play_sound()
        if event.keysym in ["c", "C"]:
            self.clear()
        if event.keysym in ["r", "R"]:
            self.record_sound()
        if event.keysym in ["minus", "KP_Subtract"]:
            self.scale_grey.set(self.grey-0.1)
        if event.keysym in ["plus", "KP_Add"]:
            self.scale_grey.set(self.grey+0.1)
        if event.keysym in ["Left"]:
            self.scale_size.set(self.circle_size-1)
        if event.keysym in ["Right"]:
            self.scale_size.set(self.circle_size+1)


spectro = Spectro()

tkinter.mainloop()
