# TTS: Trace Ton Son // Trace Thy Sound  // Draw Your Sound

Toy software to play with time-frequency representations and listen to the corresponding signal.

PC version. Android version is [here](android/).

## Installation
- Requires Python3
- Requires Python libraries numpy, librosa, sounddevice, openvCV (cv2) and PIL
- Requires a speaker or a headphone
- (optionnal) a microphone

## Usage
- Click and move the mouse to draw your sound
- Play button to play the associated sound
- Eraser toggle button to remove parts of the drawn signal
- Record button to record your own 10s-signal
- Clear button resets
- Quit button to quit

### keybindings
- "p" to play
- "r" to record
- "c" to clear
- "q" to quit
- right click to toggle Eraser

## Screenshots
![tts_drawing.png](tts_drawing.png)

![tts_record.png](tts_record.png)
