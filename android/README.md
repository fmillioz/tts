# TTS: Trace Ton Son // Trace Thy Sound  // Draw Your Sound
Android version, in debug mode. Uses [kivy](https://kivy.org) as User Interface.

## Installation
Download and install the [apk](https://www.creatis.insa-lyon.fr/nextcloud/index.php/s/2rsWWnXHdReg6Yk). (Link to the [apk in French](https://www.creatis.insa-lyon.fr/nextcloud/index.php/s/E7jQjA9jqdzQ46r))

## Packaging
The proposed apk has been packaged using [Buildozer](https://buildozer.readthedocs.io/en/latest/).
```
buildozer -v android debug
```

## Limitations
Android version does not propose sound recording.
