#!/usr/bin/env python3
import cv2
import wave
import numpy as np
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.label import Label
from kivy.graphics.texture import Texture
from kivy.core.audio import SoundLoader
from kivy.properties import ObjectProperty, StringProperty, NumericProperty
from kivy.clock import Clock
from kivy.animation import Animation

from android.storage import app_storage_path
settings_path = app_storage_path()
#settings_path = "./"

FS = 16000
HIGH_FS = 44100

class TTS(Image):
    help_text = StringProperty(None)
    red_bar = ObjectProperty(None)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.draw_size = 3
        self.draw_colour = 0
        self.cmap = ["Blanc"]
        self.cmap.extend(["Gris clair"]*3)
        self.cmap.extend(["Gris"]*3)
        self.cmap.extend(["Gris foncé"]*3)
        self.cmap.extend(["Noir"])
        self.fit_mode = "fill"
        self.rubber = False
        self.oldx, self.oldy = None, None
        self.help_display = False
        self.help_text_static = ("  Trace Ton Son v0.2\n\n" +
                                 "Play: compute and play the sound\n" +
                                 "Stop: stop the sound\n" +
                                 "Size: size of the line drawing\n" +
                                 "Colour: change the sound intensity\n" +
                                 "Clear: reset the screen\n" +
                                 "Eraser: switch to eraser\n" +
                                 "Help: display this help\n\n" +
                                 "https://framagit.org/fmillioz/tts")
        self.help_text = ""

        # STFT parameters
        self.fs = FS
        self.high_fs = HIGH_FS
        self.n_fft = 1024
        self.hop_length = 187
        self.win_length = 1012
        n = np.arange(self.win_length)/self.win_length-0.5
        hann_win = 0.5 + 0.5*np.cos(2*np.pi*n)
        self.fft_window = np.pad(hann_win, (self.n_fft - len(hann_win))//2)

        self.sig = np.zeros(10*self.fs)  # 10s blank signal
        self.stft()                      # blank STFT
        self.resolution = self.S.shape   # to get its resolution
        self.sig_flag = False
        self.sound = None
        self.scheduled = []              # list of scheduled events
        self.red_line_points = [None, None]
        self.mask = np.ones(self.resolution)
        self.texture = Texture.create(size=np.flip(self.resolution),
                                      colorfmt='luminance')
        self.texture_size = list(self.texture.size)
        self.update()

    def update(self):
        img = np.uint8(self.mask/self.mask.max()*255)
        self.texture.blit_buffer(img.tobytes(),  bufferfmt="ubyte",
                                 colorfmt="luminance")
        self.canvas.ask_update()

    def on_touch_down(self, touch):
        self.oldx, self.oldy = self.touch2maskxy(touch)

    def on_touch_move(self, touch):
        if self.collide_point(touch.x, touch.y):
            x, y = self.touch2maskxy(touch)
            val = self.draw_colour if not self.rubber else 1
            draw_size = self.draw_size if not self.rubber else self.draw_size+9
            draw_size = int(draw_size)
            if x and y and self.oldx and self.oldy:
                cv2.line(self.mask, (self.oldx, self.oldy), (x, y), val,
                         draw_size)
                self.oldx, self.oldy = x, y

        self.sig_flag = True  # we have to recompute signal
        self.update()

    def touch2maskxy(self, touch):
        x = int(touch.x/self.width*self.resolution[1])
        y = int(touch.y/self.height*self.resolution[0])
        return x, y

    def change_rubber(self):
        self.rubber = not(self.rubber)

    def change_size(self, value):
        self.draw_size = value
        self.oldx, self.oldy = None, None  # reinit position

    def change_colour(self, value):
        self.draw_colour = 1 - value/10
        self.oldx, self.oldy = None, None  # reinit position

    def clear(self):
        self.stop(None)
        self.mask = np.ones(self.resolution)
        self.sig = np.zeros(10*self.fs)  # 10s signal
        self.sig_flag = False
        self.update()

    def griffinlim(self, absS):
        # inspired and simplified from librosa
        n_iter = 3
        momentum = 0.99
        # random initial phases
        angles = np.exp(2j * np.pi * np.random.rand(*self.S.shape))
        # And initialize the previous iterate to 0
        previous = np.zeros(self.S.shape)

        for i in range(n_iter):
            # Invert with our current estimate of the phases
            self.S = absS*angles
            self.istft()
            # Rebuild the spectrogram
            self.stft()

            # Update our phase estimates
            angles[:] = self.S - (momentum / (1 + momentum)) * previous
            angles[:] /= np.abs(angles) + 1e-16

            # Store the previous iterate
            previous = self.S

        # Return the final phase estimates
        self.S = absS*angles
        self.istft()
        self.sig /= np.abs(self.sig).max()  # normalized

    def stft(self):
        s_pad = np.pad(self.sig, self.n_fft//2)
        start = np.arange(len(s_pad)-self.win_length, step=self.hop_length)
        S = np.zeros((self.n_fft//2+1, len(start)))*1j
        for idx, istart in enumerate(start):
            sig_extract = s_pad[istart:istart+self.n_fft]*self.fft_window
            S[:, idx] = np.fft.rfft(sig_extract)
        self.S = S

    def istft(self):
        sig_rec = np.zeros(10*self.fs + self.n_fft)
        win_sum = np.zeros(10*self.fs + self.n_fft)
        start = np.arange(self.S.shape[1])*self.hop_length
        for idx, istart in enumerate(start):
            extract = np.real(np.fft.irfft(self.S[:, idx])) * self.fft_window
            sig_rec[istart:istart+self.n_fft] += extract   # overlap-add
            win_sum[istart:istart+self.n_fft] += self.fft_window ** 2
        positions = win_sum != 0
        sig_rec[positions] = sig_rec[positions]/(win_sum[positions] + 1e-16)
        # remove padding
        self.sig = np.zeros(10*self.fs+self.n_fft)
        self.sig[:len(sig_rec[self.n_fft//2:])] = sig_rec[self.n_fft//2:]
        self.sig = self.sig[:10*self.fs]

    def spectro2sound(self):
        S = 1 - self.mask
        S = 10**(S)
        S -= S.min()
        self.griffinlim(S)
        self.sig_flag = False

    def resample(self):
        # simple signal resampling to avoid strange zero-holder-block effect
        # simple: only odd number of points, big approximations
        if len(self.sig)%2 == 0:
            sig = self.sig[:-1]
        else:
            sig = self.sig
        SIG = np.fft.fftshift(np.fft.fft(sig))
        K = 441000  # len(sig) * self.high_fs / self.fs
        zeros = np.zeros(int((K-len(SIG))/2))
        SIG_pad = np.hstack((zeros, SIG, zeros))
        sig_high_fs = np.fft.ifft(np.fft.ifftshift(SIG_pad))
        # normalization
        sig_high_fs = sig_high_fs * len(sig_high_fs) / len(sig)
        return sig_high_fs.real  

    def play(self):
        if not self.sound:
            if self.sig_flag:
                self.spectro2sound()
            if np.sum(np.abs(self.sig)):
                with wave.open(settings_path+"/tts.wav", "w") as f:
                    f.setnchannels(1)
                    # 2 bytes per sample.
                    f.setsampwidth(2)
                    f.setframerate(self.high_fs)
                    # wave only use integers (?)  // resampling to ~44kHz
                    s_int16 = (self.resample()*(2**15-1)).astype("int16")
                    f.writeframes(s_int16.tobytes())
                self.sound = SoundLoader.load(settings_path+"/tts.wav")
                if self.sound:
                    self.sound.play()
                    self.red_bar.move()
                    self.scheduled.append(Clock.schedule_once(self.stop, 10.5))
        else:
            self.stop(None)  # a useless parameter for Clock.schedule_once
            self.play()

    def stop(self, _):
        if self.sound:
            self.sound.stop()
            self.sound = None
            self.red_bar.remove(0)
            for s in self.scheduled:
                Clock.unschedule(s)
            self.scheduled = []

#    def record(self, _):
#       kivy sound recipe
#       signal.lfilter
#        pass

    def help(self):
        if self.help_text == self.help_text_static:
            self.help_text = ""
        else:
            self.help_text = self.help_text_static


class RedBar(Widget):
    def move(self):
        self.x = 0
        self.anim = Animation(x=self.parent.width, duration=10)
        self.anim.start(self)

    def remove(self, dt):
        self.x = -1000
        self.anim.stop(self)


class FreqAxis(RelativeLayout):
    lbl = ObjectProperty(None)

    def on_lbl(self, *args):
        for i in range(11):
            lbl = Label()
            freq = FS/2000*i/10
            if freq != round(freq):
                lbl.text = f'{freq:.1f}'  # one decimal
                lbl.pos_hint = {'x': -0.1, 'y': i/10-0.5}
            else:
                lbl.text = str(int(freq))
                lbl.pos_hint = {'x': 0, 'y': i/10-0.5}

            lbl.color = (0, 0, 0, 1)
            if i == 10:
                lbl.pos_hint = {'x': 0, 'y': 0.49}
            self.lbl.add_widget(lbl)


class TimeAxis(RelativeLayout):
    lbl = ObjectProperty(None)

    def on_lbl(self, *args):
        for i in range(11):
            lbl = Label(text=(str(i)))
            lbl.color = (0, 0, 0, 1)
            lbl.pos_hint = {'x': i/10-0.5, 'y': -0.5}
            if i == 10:
                lbl.pos_hint = {'x': 0.49, 'y': -0.5}
            self.lbl.add_widget(lbl)


class HelpText(Label):
    pass


class TTSScreen(BoxLayout):
    btn_size = 120

    def time_axis_points(self, width, height):
        dx = width/10
        points = []
        points.append(0)
        points.append(height-self.btn_size/4)
        points.append(0)
        points.append(height)
        for i in range(10):
            points.append(i*dx)
            points.append(height)
            points.append((i+1)*dx)
            points.append(height)
            points.append((i+1)*dx)
            points.append(height-self.btn_size/4)

        return points

    def freq_axis_points(self, width, height):
        dy = height/10
        points = []
        points.append(width-self.btn_size/10)
        points.append(0)
        points.append(width)
        points.append(0)
        for i in range(10):
            points.append(width)
            points.append(i*dy)
            points.append(width)
            points.append((i+1)*dy)
            points.append(width-self.btn_size/10)
            points.append((i+1)*dy)

        return points


class TraceTonSonApp(App):
    tts = ObjectProperty(None)

    def build(self):
        return TTSScreen()


if __name__ == '__main__':
    TraceTonSonApp().run()
